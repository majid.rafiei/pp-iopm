## Introduction
This project implements an abstraction-based approach for privacy-aware federated process mining.
## Python package
The implementation has been published as a standard Python package. Use the following command to install the corresponding Python package:

```shell
pip install pp-iopm
```

## Usage
Look at the "main_....py" files to see the usage example.
