import os
from pm4py.objects.log.importer.xes import importer as xes_importer
import pm4py
from pp_iopm.Connector import Connector
from pp_iopm.IOPM import IOPM


current_dir = os.path.dirname(os.path.realpath(__file__))
log_name = "artificial.xes"
log_path = os.path.join(current_dir, 'event_logs', log_name)
log = xes_importer.apply(log_path)
log = pm4py.insert_artificial_start_end(log)
connector_obj = Connector(log)
basic_conn_all, case_ids = connector_obj.convert_to_basic_connector()


number_of_org = 3
iopm = IOPM(number_of_org)
org_names = ["1","2","3"]
org_activities = [["a","b","c","m","n"],
                  ["d","e","f"],
                  ["g","h","k"]
                  ]
iopm.define_orgs(org_names,org_activities)
handover_tables = iopm.create_handover_tables(log)


handover_relations, handover_to, handover_from = iopm.discover_handover_relations(handover_tables, case_ids)


log_name = "artificial_1.xes"
log_path = os.path.join(current_dir, 'event_logs', log_name)
log_1 = xes_importer.apply(log_path)
log_1 = pm4py.insert_artificial_start_end(log_1)
connector_obj = Connector(log_1)
basic_conn_1, cases_1 = connector_obj.convert_to_basic_connector()

log_name = "artificial_2.xes"
log_path = os.path.join(current_dir, 'event_logs', log_name)
log_2 = xes_importer.apply(log_path)
log_2 = pm4py.insert_artificial_start_end(log_2)
connector_obj = Connector(log_2)
basic_conn_2,cases_2 = connector_obj.convert_to_basic_connector()

log_name = "artificial_3.xes"
log_path = os.path.join(current_dir, 'event_logs', log_name)
log_3 = xes_importer.apply(log_path)
log_3 = pm4py.insert_artificial_start_end(log_3)
connector_obj = Connector(log_3)
basic_conn_3,cases_3 = connector_obj.convert_to_basic_connector()



dict_freq, dict_start, dict_end = connector_obj.convert_list_to_dict(basic_conn_all)
pm4py.view_dfg(dict(dict_freq), dict(dict_start), dict(dict_end), format="png")


cuel_without_handover = basic_conn_1 + basic_conn_2 + basic_conn_3
dict_freq, dict_start, dict_end = connector_obj.convert_list_to_dict(cuel_without_handover)
pm4py.view_dfg(dict(dict_freq), dict(dict_start), dict(dict_end), format="png")


cuel_with_handover = iopm.update_cuel_subcon_scm(cuel_without_handover, handover_relations)
dict_freq, dict_start, dict_end = connector_obj.convert_list_to_dict(cuel_with_handover)
pm4py.view_dfg(dict(dict_freq), dict(dict_start), dict(dict_end), format="png")


print("Done!")