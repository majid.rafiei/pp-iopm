import os
from pm4py.objects.log.importer.xes import importer as xes_importer
import pm4py
from pp_iopm.Connector import Connector
from pp_iopm.IOPM import IOPM


current_dir = os.path.dirname(os.path.realpath(__file__))
log_name = "Sepsis_scenarios.xes"
log_path = os.path.join(current_dir, 'event_logs', log_name)
log = xes_importer.apply(log_path)
log = pm4py.insert_artificial_start_end(log)
connector_obj = Connector(log)
basic_conn_all, case_ids = connector_obj.convert_to_basic_connector()


iopm = IOPM(2)
org_names = ["ER","LB"]
org_activities = [["ER Registration", "ER Triage", "ER Sepsis Triage","Release A", "Release B", "Release C", "Release D", "Release E", "Return ER"],
                  ["IV Liquid", "IV Antibiotics", "Admission NC", "Admission IC", "LacticAcid", "CRP", "Leucocytes"],
                  ]
iopm.define_orgs(org_names,org_activities)
handover_tables = iopm.create_handover_tables(log)


handover_relations, handover_to, handover_from = iopm.discover_handover_relations(handover_tables, case_ids)


log_name = "Sepsis_scenarios_ER_FI.xes"
log_path = os.path.join(current_dir, 'event_logs', log_name)
log_ER_FI = xes_importer.apply(log_path)
log_ER_FI = pm4py.insert_artificial_start_end(log_ER_FI)
connector_obj = Connector(log_ER_FI)
basic_conn_ER_FI, cases_ER_FI = connector_obj.convert_to_basic_connector()

log_name = "Sepsis_scenarios_LB.xes"
log_path = os.path.join(current_dir, 'event_logs', log_name)
log_LB = xes_importer.apply(log_path)
log_LB = pm4py.insert_artificial_start_end(log_LB)
connector_obj = Connector(log_LB)
basic_conn_LB,cases_LB = connector_obj.convert_to_basic_connector()


dict_freq, dict_start, dict_end = connector_obj.convert_list_to_dict(basic_conn_all)
pm4py.view_dfg(dict(dict_freq), dict(dict_start), dict(dict_end), format="pdf")


cuel_without_handover = basic_conn_ER_FI + basic_conn_LB
dict_freq, dict_start, dict_end = connector_obj.convert_list_to_dict(cuel_without_handover)
pm4py.view_dfg(dict(dict_freq), dict(dict_start), dict(dict_end), format="pdf")


cuel_with_handover, remaining_hor = iopm.update_cuel_subcon(cuel_without_handover,handover_relations)
dict_freq, dict_start, dict_end = connector_obj.convert_list_to_dict(cuel_with_handover)
pm4py.view_dfg(dict(dict_freq), dict(dict_start), dict(dict_end), format="pdf")


print("Done!")