from pp_iopm import Connector,Handover,IOPM,Organization

__name__ = 'pp_iopm'
__version__ = '0.0.1'
__doc__ = "An Abstraction-Based Approach for Privacy-Aware Inter-Organizational Process Mining"
__author__ = 'Majid Rafiei'
__author_email__ = 'majid.rafiei@pads.rwth-aachen.de'
__maintainer__ = 'Majid Rafiei'
__maintainer_email__ = "majid.rafiei@pads.rwth-aachen.de"